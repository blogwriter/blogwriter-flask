from flask import (
    Blueprint,
    request,
    jsonify,
    g,
)
from flask_jwt_extended import (
    create_access_token,
    get_jwt_identity,
    jwt_required,
    set_access_cookies,
)
from sqlalchemy import exc
import blogwriter.database as db
from blogwriter.models.user import User


bp = Blueprint('auth', __name__)


@bp.post("/register")
def register():
    req_data: dict = request.get_json()
    user_name: str = req_data.get("user_name")
    password: str = req_data.get("password")

    if not user_name:
        return {"error": "User name is required"}
    if not password:
        return {"error": "Password is required"}

    try:
        user = User(user_name)
        user.hash_password(password)
        db.save(user)
    except exc.IntegrityError:
        return {"error": f"User {user_name} is laready registered."}

    return {"success": "you created an account"}, 201


@bp.post('/login')
def login():
    req_data: dict = request.get_json()
    user_name: str = req_data.get("user_name")
    password: str = req_data.get("password")
    user = User.query.filter_by(user_name=user_name).first()
    if not user:
        return {"error": "Incorrect user name."}
    if not user.verify_password(password):
        return {"error": "Incorrect password."}

    access_token = create_access_token(identity=user.user_name)
    response = jsonify({
        "user_name": user.user_name,
        "access_token": access_token,
    })
    set_access_cookies(response, access_token)
    return response, 201
