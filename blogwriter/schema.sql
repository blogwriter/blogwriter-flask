DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS message;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE message (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user_id INTEGER NOT NULL,
  role TEXT NOT NULL,
  content TEXT NOT NULL,
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES user (id)
);

INSERT INTO user (username, password) VALUES ('user1', 'user1');

INSERT INTO user (username, password) VALUES ('user2', 'user2');

INSERT INTO message (user_id, role, content) VALUES (1, 'user', 'hello world');
