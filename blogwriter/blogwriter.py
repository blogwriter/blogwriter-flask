from flask import (
    Blueprint, request,
)
from flask_jwt_extended import jwt_required

from blogwriter.utils.chatgpt_helper import chatgpt
from blogwriter import database
from blogwriter.models.message import Message
from blogwriter.constants import (
    ASSISTANT_ROLE,
    USER_ROLE,
)


bp = Blueprint('blogwriter', __name__)


@bp.get("/test_jwt")
@jwt_required()
def auth_test():
    return {"response": "success auth"}


@bp.post('/chat')
def chat():
    req_data: dict = request.get_json()
    user_msg: str = req_data.get("user_msg")
    chatgpt_key: str = req_data.get("chatgpt_key")

    # grab message history from database.
    msgs = database.query(Message)
    msg_history: list[dict] = []
    for msg in msgs:
        msg_history.append({
            "role": msg.role,
            "content": msg.content
        })

    print(msg_history)
    # interact with chatgpt
    reply_content = chatgpt(msg_history, chatgpt_key)
    msg_history.append({"role": ASSISTANT_ROLE, "content": f"{reply_content}"})

    # persist the user message and chatgpt response to database
    msg_models = [Message(1, USER_ROLE, user_msg),
                  Message(1, ASSISTANT_ROLE, reply_content)]

    database.save(msg_models)
    return {
        "reply":
            {
              "role": ASSISTANT_ROLE, "content": reply_content
            }
    }
