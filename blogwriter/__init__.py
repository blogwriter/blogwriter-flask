import os

from flask import Flask
from flask_cors import CORS
from . import database
from blogwriter.models.user import User
from blogwriter.models.message import Message
from flask_jwt_extended import JWTManager


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE=os.path.join(app.instance_path, "blogwriter.sqlite"),
        SQLALCHEMY_DATABASE_URI=os.path.join(
            app.instance_path, "blogwriter.sqlite"),
        JWT_TOKEN_LOCATION=["cookies"],
    )

    # Setup the Flask-JWT-Extended extension
    app.config["JWT_SECRET_KEY"] = "super-secret"  # Change this!
    JWTManager(app)

    if test_config is None:
        # Load the instance config, if it exists, when not testing
        app.config.from_pyfile("config.py", silent=True)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # create the database extension
    # configure the SQLite database, relative to the app instance folder
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///blogwriter.sqlite"
    # initialize the app with the extension
    database.init_app(app)

    from . import blogwriter
    app.register_blueprint(blogwriter.bp)

    from .import auth
    app.register_blueprint(auth.bp)

    # enable CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    # simple page that says hello
    @app.route("/hello")
    def hello():
        return "hello, world"

    @app.route('/testdb')
    def testdb():
        try:
            data = database.query(Message)
            return f'<h1>It works.-->{data} </h1>'
        except Exception as e:
            # e holds description of the error
            error_text = "<p>The error:<br>" + str(e) + "</p>"
            hed = '<h1>Something is broken.</h1>'
            return hed + error_text

    return app
