from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, text
import click
from flask import current_app, g


db = SQLAlchemy()


def init_db():
    # engine = create_engine("sqlite:///instance/blogwriter.sqlite", echo=True)
    # with engine.connect() as con:
    #     with current_app.open_resource('schema.sql') as f:
    #         queries: list = f.read().decode('utf8').split(";")
    #         for query in queries:
    #             con.execute(text(query))
    #             con.commit()
    db.drop_all()
    db.create_all()
    db.session.commit()


def init_app(app):
    db.init_app(app)
    app.cli.add_command(init_db_command)


def query(model):
    return db.session.query(model).all()
    # return db.session.query(text('1')).from_statement(text('SELECT 1')).all()


def save(models):
    for model in models:
        db.session.add(model)
    db.session.commit()


@click.command('init-db')
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')
