import gitlab


def commit(content, filename, project_namespace, private_token):
    gl = gitlab.Gitlab(private_token=private_token)
    # Get a project by name with namespace
    project = gl.projects.get(project_namespace)
    print(project.name)

    data = {
        'branch': 'main',
        'commit_message': f'adding a new file: {filename}',
        'actions': [
            {
                'action': 'create',
                'file_path': filename,
                'content': content
            },
        ]
    }

    project.commits.create(data)