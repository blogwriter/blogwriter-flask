from typing import List
import openai


# set our key


def chatgpt(msg_history: List[dict], api_key: str) -> str:
    openai.api_key = api_key
    completion = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=msg_history
    )
    reply_content: str = completion.choices[0].message.content
    return reply_content
